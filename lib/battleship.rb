require_relative 'board'
require_relative 'player'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
    2.times { board.place_random_ship }
  end

  def play
    until game_over?
      display_status
      play_turn
    end

    display_status
    puts "#{player.name} you sank my battleship!"
  end

  def attack(pos)
    # x is a hit, o is a miss
    board[pos] == :s ? (board[pos] = :x) : (board[pos] = :o)
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def display_status
    board.display
    puts "#{board.count} ships left"
    puts
  end

  def play_turn
    begin
      move = player.get_play(board.grid.length)
    rescue
      puts 'Please input a valid move.'
      retry
    end
    self.attack(move)
  end

  def play
    until game_over?
      display_status
      play_turn
    end

    display_status
    puts "#{player.name} you sank my battleship!"
  end
end

if __FILE__ == $PROGRAM_NAME
  print 'What is your name: '
  player1 = gets.chomp
  print 'What size square board do you want? Input length: '
  size = gets.chomp.to_i

  grid = Array.new(size) { Array.new(size) {nil} }
  player = HumanPlayer.new(player1)
  board = Board.new(grid)

  game = BattleshipGame.new(player, board)
  game.play

end
