class Board
  attr_reader :default_grid
  attr_accessor :grid

  def self.default_grid
    Array.new(10) { Array.new(10) {nil} }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, mark)
    x, y = pos
    @grid[x][y] = mark
  end

  def display

    print '  '
    @grid.length.times do |col|
      print " #{col}  "
    end
    puts

    @grid.each_with_index do |row, idx|
      print "#{idx}|"
      row.each do |mark|
        if [:x, :o].include?(mark)
          print " #{mark} |"
        else
          print '   |'
        end
      end
      print "\n"
    end
  end

  def count
    count = 0
    @grid.each do |row|
      row.each do |mark|
        count += 1 if mark == :s
      end
    end
    count
  end

  def empty?(pos = nil)
    # when no position is passed, check if any ships on board
    return count.zero? if pos.nil?
    # when passed position, check if that position is empty
    self[pos].nil?
  end

  def full?
    @grid.flatten.all? { |mark| mark }
  end

  def in_range?(pos)
    (pos[0] >= 0 && pos[0] < @grid[0].length) &&
    (pos[1] >= 0 && pos[1] < @grid.length)
  end

  def place_random_ship
    raise puts 'board full' if full?

    pos = self.random_pos
    pos = self.random_pos until empty?(pos)

    self[pos] = :s
  end

  def won?
    count.zero?
  end

  protected

  def random_pos
    size = @grid.length
    [rand(size), rand(size)]
  end

end
