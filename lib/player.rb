class HumanPlayer
  attr_accessor :name

  def initialize(player_name = 'Human')
    @name = player_name
  end

  def get_play(board_size)
    print 'Input your move - x,y: '
    move = gets.chomp
    x = move[0].to_i
    y = move[-1].to_i

    raise unless [x,y].all? { |i| i >= 0 && i < board_size }

    [x, y]
  end
end

class ComputerPlayer
  attr_accessor :name

  def initialize(player_name = 'Computer')
    @name = player_name
  end

  def get_play(board_size)
    guesses = []
    x = rand(board_size - 1)
    y = rand(board_size - 1)
    while guesses.include?([x, y])
      x = rand(board_size - 1)
      y = rand(board_size - 1)
    end
    guesses << [x, y]

    [x, y]
  end
end
